<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/hello', function () {
	return "Hello";
});

Route::group([ 'middleware' => ['auth:api', 'role:administrator'] ], function() {
	Route::post('/create-prof', 'UserController@createProfessional');
	Route::post('/update-prof', 'UserController@updateProfessional');
	Route::post('/delete-prof', 'UserController@deleteProfessional');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
