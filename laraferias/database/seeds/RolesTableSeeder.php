<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new Role;
        $administrator->name = 'administrator';
        $administrator->description = 'Administrador do sistema';
        $administrator->save();

        $administrator = new Role;
        $administrator->name = 'professional';
        $administrator->description = 'Profissional da Clínica';
        $administrator->save();
    }
}
