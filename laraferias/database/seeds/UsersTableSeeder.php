<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new User;
        $administrator->name = 'Administrator';
        $administrator->email = 'admin@ecomp.co';
        $administrator->password = bcrypt('secret');
        $administrator->save();
        $administrator->roles()->attach(Role::administrator());

        $professional = new User;
        $professional->name = 'Profissional';
        $professional->email = 'professional@ecomp.co';
        $professional->password = bcrypt('123456');
        $professional->save();
        $professional->roles()->attach(Role::professional());
    }
}
