<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Role extends Model
{
    public function users()
    {
    	return $this->belongsToMany(User::class);
    }

    public static function administrator()
    {
    	return self::where('name', 'administrator')->first();
    }

    public static function professional()
    {
    	return self::where('name', 'professional')->first();
    }
}
