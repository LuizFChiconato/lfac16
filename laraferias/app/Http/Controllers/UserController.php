<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Role;
use App\Http\Requests\UserCreateProfessionalRequest;
use App\Http\Requests\UserUpdateProfessionalRequest;

class UserController extends Controller
{
    public function createProfessional(UserCreateProfessionalRequest $request)
    {
    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->save();
    	$user->roles()->attach(Role::professional());

    	return $user;
    }

    public function getProfessionals()
    {
    	return User::professionals;
    }

    public function getProfessional(Request $request, $id)
    {
    	return User::find($id);
    }

    public function updateProfessional(UserUpdateProfessionalRequest $request)
    {
    	$user = User::findOrFail($request->id);
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->save();

    	return $user;
    }

    public function deleteProfessional(Request $request)
    {
    	$user = User::findOrFail($request->id);
    	$user->delete();
    	return $user;
    }
}
